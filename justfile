zola_bin := "flatpak run org.getzola.zola"
tailwind_bin := "npx tailwindcss"

zola:
	{{zola_bin}} serve --open --drafts

tailwind:
	{{tailwind_bin}} -mw -i input.css -o static/main.css
