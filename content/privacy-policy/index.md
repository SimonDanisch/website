+++
title = "Privacy Policy"
+++

The website `mo8it.com` does not collect any personal data of its users.

The website also does not store any cookies.
