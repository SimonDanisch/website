+++
title = "Oxiform - Self-hosted forms in Rust"
description = "Presenting Oxiform for self-hosted forms for your own websites written in Rust"
date = 2023-04-07
draft = true
[taxonomies]
tags = ["rust", "web", "self-hosting"]
+++

<!-- more -->

<!-- toc -->
