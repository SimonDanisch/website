+++
title = "Getting started with Rust backends"
description = "A tutorial for getting started with web backends in Rust with the Axum framework"
date = 2023-04-01
updated = 2023-05-29
[taxonomies]
tags = ["rust", "axum", "web"]
+++

How hard can it be to write a web backend in Rust? We will try to answer this question through an example.

<!-- more -->

The example will be a simple HTML contact form with the following elements:

- Name field
- Email field
- Message field
- Submit button
- Error message

The web server will serve this form and handle its submission.
In case the form submission was not valid, the form is returned back without losing the user's input.
Otherwise, the user receives a confirmation that the contact form was submitted successfully.

<!-- toc -->

#### Disclaimer

If you are a beginner, you can take this post as a demonstration of what you can achieve with a basic Rust backend, but don't get distracted by details 😵‍💫

This is not a HTML/CSS tutorial. The HTML pages will be ugly, but _functional_.
One can add design details later 🎨 Personally, I would recommend [Tailwind CSS](https://tailwindcss.com).

## Dependencies

The example uses the following crates:

- [axum](https://docs.rs/axum/0.6.18/axum): Web framework
- [askama](https://docs.rs/askama/0.12.0/askama): Templating engine with compile time template validation
- [tokio](https://docs.rs/tokio/1.28.2/tokio): Asynchronous runtime
- [serde](https://docs.rs/serde/1.0.163/serde): (De)Serialization

To follow the example, create a new Rust project with `cargo new` and add the following dependencies to `Cargo.toml`:

```toml
[dependencies]
askama = "0.12.0"
axum = "0.6.18"
serde = { version = "1.0.163", features = ["derive"] }
tokio = { version = "1.28.2", features = ["macros", "rt-multi-thread"] }
```

### Imports

All imports used in the example are gathered below to keep later code snippets slim:

```rust
use askama::Template;
use axum::{
    extract::Form,
    http::StatusCode,
    response::{Html, IntoResponse, Response},
    routing::{get, post, Router},
    Server,
};
use serde::Deserialize;
```

## Server

Let's finally start building the server!

In the following snippet, we build a test router that returns `Hello world!`.
Then we build the server to serve the router on `localhost`.

```rust
#[tokio::main]
async fn main() {
    let router = Router::new()
        .route("/", get(|| async { "Hello world!" }));

    let socket_address = "127.0.0.1:8080"
        .parse()
        .expect("Failed to parse the socket address!");

    Server::bind(&socket_address)
        .serve(router.into_make_service())
        .await
        .expect("Failed to start the server!");
}
```

Run the program and visit `127.0.0.1:8080` in your browser to see the `Hello world!` message.

Congratulations, you did just build your first Rust web server 🎉

{% note() %}
You need to use the socket address `0.0.0.0` (with some port) when you want to host the server in a container for example.
{% end %}

## Form template

Now, we want to create the form template to serve it instead of `Hello world!`.

First, create the directory `templates` where all templates will be placed (default directory for `askama`).

Write the following form template in the file `templates/form.html`:

```html
<!doctype html>
<html lang="en">
    <body>
        <form action="/submit" method="post">
            <div>
                <label for="name">Name</label>
                <input type="text" name="name" id="name" value="{{ name }}" required/>
            </div>

            <div>
                <label for="email">Email</label>
                <input type="email" name="email" id="email" value="{{ email }}" required/>
            </div>

            <div>
                <div>
                    <label for="message">Message</label>
                </div>
                <textarea name="message" id="message" required>{{ message }}</textarea>
            </div>

            <button type="submit">Submit</button>
        </form>
    </body>
</html>
```

This template produces a form like the one below:

<div style="margin-top: 2rem; margin-bottom: 2rem;">
    <div style="margin-bottom: 0.4rem;">
        <label for="name">Name</label>
        <input type="text" id="name" value="Ferris" required/>
    </div>
    <div style="margin-bottom: 0.4rem;">
        <label for="email">Email</label>
        <input type="email" id="email" value="ferris@rust-lang.org" required/>
    </div>
    <div style="margin-bottom: 0.4rem;">
        <div>
            <label for="message">Message</label>
        </div>
        <textarea id="message" required>Are we web yet?</textarea>
    </div>
    <button style="padding: 0.2rem; border-width: 1px;">Submit</button>
</div>

Did you notice the three variables inside `{{ }}`?
These placeholders will be replaced by the values provided to the template.

Now, we define a struct representing the template in our Rust code and having the variables that we did use as placeholders in our template:

```rust
#[derive(Template, Default)]
#[template(path = "form.html")]
struct FormTemplate<'a> {
    name: &'a str,
    email: &'a str,
    message: &'a str,
}
```

`path` points to the template's path inside of the `templates` directory.

Note that we did derive the trait `Default` to be able to create the template with empty strings as the default.

## Rendering templates

To serve the template, it has to be rendered and returned as a response by a handler.

Because rendering a template and returning it as a response is common in a backend,
we want to separate this logic into a function that renders a template and returns it as HTML:

```rust
fn render_template(template: impl Template) -> Response {
    match template.render() {
        Ok(rendered) => Html(rendered).into_response(),
        Err(e) => {
            eprintln!("Failed to render template: {e:?}");

            StatusCode::INTERNAL_SERVER_ERROR.into_response()
        }
    }
}
```

In case the template could not be rendered, the status code 500 is returned as a representation of an internal server error.

Now, we can use this function to render our form template.

## Index handler

The handler responsible of rendering our form template is very simple:

```rust
async fn index() -> Response {
    let template = FormTemplate::default();
    render_template(template)
}
```

First, we initialize the templates with default values since the default of `String` is an empty string.
Then we return the response of our template rendering function.

Now, we have to let this handler handle `GET` requests to our index path. This is done by replacing the `Hello World!` closure in the `main` function with `get(index)`:

```rust
let router = Router::new()
    .route("/", get(index));
```

Now, run the program and visit `127.0.0.1:8080` in your browser to see the form 🎉

You can fill it, but submitting will not work because we are not handling form submissions yet.

## Success template

Let's prepare for handling a form submission!

After a successful form submission, we want to give the user a feedback.
Therefore, we create the template `templates/success.html`:

```html
<!doctype html>
<html lang="en">
    <body>
        <p>Thank you for contacting us, {{ name }}. We will reach out to you as soon as possible.</p>
    </body>
</html>
```

The template takes the name of the user as a template variable. This has to be reflected in the struct of this template:

```rust
#[derive(Template)]
#[template(path = "success.html")]
struct SuccessTemplate<'a> {
    name: &'a str,
}
```

{% note() %}
In a real project, you should use a [base template](https://djc.github.io/askama/template_syntax.html#child-template) to prevent duplication!
{% end %}

## Form deserialization

Before returning the success template, we have to receive the form.
To do so, we write a deserialization struct for our form with the expected input fields:

```rust
#[derive(Deserialize)]
struct FormFields {
    name: String,
    email: String,
    message: String,
}
```

The deserialization struct can now be used with the form [extractor](https://docs.rs/axum/0.6.18/axum/extract) `Form` provided by `axum` that tries to deserialize a request form into this struct.
This extractor will be the only argument of our form submission handler:

```rust
async fn submit(fields: Form<FormFields>) -> Response {
    if fields.name.len() < 2 || fields.email.len() < 3 || !fields.email.contains('@') {
        let template = FormTemplate {
            name: &fields.name,
            email: &fields.email,
            message: &fields.message,
        };
        return render_template(template);
    }

    println!(
        "Submission:\nName: {}\nEmail: {}\nMessage: {}\n",
        &fields.name, &fields.email, &fields.message
    );

    let template = SuccessTemplate { name: &fields.name };
    render_template(template)
}
```

First, we do some server side form validation. **This is not a good email validation!** It is just a placeholder for demonstration.

If the submitted form is not valid, we return the form template back, but this time with the fields from the form so that the user does not lose his form input.

If the form is valid, we print the submitted fields to stdout just as a placeholder for some proper handling of a successful contact.

Now, let's make this handler handle `POST` requests to the `/submit` path (the path that `action` in our form template points to):

```rust
let router = Router::new()
    .route("/", get(index))
    .route("/submit", post(submit));
```

## Test

Now that we did implement our two important handlers, let's test our backend.

Run the program and visit `127.0.0.1:8080` in your browser to see the form.

Fill the form and submit it to see the success feedback 🎉

Take a look at your terminal, the submission is printed there!

Now, go back and fill the form again, but this time with only one character in the name field. This will trigger the server side validation and return the form back. Although no form input is lost, the user does not know what he did wrong 😕

We need to show some error message after an invalid form.

## Error message

Let's add an error message to the form template:

```rust
struct FormTemplate<'a> {
    // ...
    error_message: &'a str,
}
```

Now, the Rust compiler will tell you that you have to do a modification in the `submit` handler:

```rust
let template = FormTemplate {
    // ...
    error_message: "Invalid input!",
};
```

The Rust compiler will also warn you that the field `error_message` is not used! This is right because we want to use it in our template file `templates/form.html`. Add the following just before the submit button:

```html
{% if !error_message.is_empty() %}
    <p>{{ error_message }}</p>
{% endif %}
```

Now, run the program and test again with only one character in the name field to see the error message 🎉

## Conclusion

We can not deny that Rust has a steep learning curve. In this blog post, we skipped many of the used key concepts in Rust like lifetimes and traits.
But after you go through the first chapters in [the Rust book](https://doc.rust-lang.org/stable/book), you will be able to start working on your own web backend in Rust.
In fact, such a contact form was my second Rust project!

Rust will even make your life easier. The compiler shifts many bugs from runtime to compile time, `serde` (de)serializes for you, `askama` makes sure your templates are valid at compile time etc. In Rust, you have an army of tools that give you the feeling that "if it compiles, it works".

On top of that, you get all the benefits of Rust regarding performance, low resource usage etc.

I hope that this post was an icebreaker for you. What are you waiting for? Let's dominate the web with Rust 😃

---

#### Full code

You can find the full code used in the post in [this repository](https://codeberg.org/mo8it/blog_demos/src/branch/main/getting-started-with-rust-backends).

#### Credits

The blog post is inpired by the following [blog post on spacedimp.com](https://spacedimp.com/blog/using-rust-axum-postgresql-and-tokio-to-build-a-blog) about building a blog in Rust.
I recommend reading it after this blog post! It contains more details and shows interacting with a database in Rust with [SQLx](https://github.com/launchbadge/sqlx).
