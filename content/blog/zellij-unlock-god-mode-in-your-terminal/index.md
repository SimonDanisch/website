+++
title = "Zellij - Unlock god mode in your terminal"
description = "How to unlock god mode in your terminal with Zellij"
date = 2023-04-27
draft = true
[taxonomies]
tags = ["terminal", "linux", "rust"]
+++

Zellij is not just a terminal multiplexer like tmux... Zellij is a terminal workspace that will boost your productivity and let you feel like a terminal wizard!

In this post, we will see how to get started with Zellij and use its powerful layouts.

<!-- more -->

<!-- toc -->

[god mode](https://hachyderm.io/@imsnif/110003989957573805)
