+++
title = "Building a Zola blog"
description = "How to build a blog with the static site generator Zola"
date = 2023-04-07
draft = true
[taxonomies]
tags = ["zola", "web", "container", "self-hosting"]
+++

In this post, I will show you how I built this blog and how you can build your own with Zola!

<!-- more -->

<!-- toc -->

## Tailwind CSS

## Containerfile

## Taxonomies

Reverse sorting tags while keeping the tag name sorted for the same `page_count`:

```
{% for page_count, terms_with_same_page_count in terms | sort(attribute="page_count") | reverse | group_by(attribute="page_count") %}
    {% for term in terms_with_same_page_count | reverse %}
        {{ term }} - {{ page_count }}
    {% endfor %}
{% endfor %}
```
