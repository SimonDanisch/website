+++
title = "Rusty terminal upgrade"
description = "Some powerful terminal tools written in Rust to upgrade your terminal"
date = 2023-04-07
draft = true
[taxonomies]
tags = ["terminal", "linux", "rust"]
+++

<!-- more -->

<!-- toc -->
