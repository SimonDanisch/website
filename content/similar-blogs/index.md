+++
title = "Similar blogs"
description = "A list of similar blogs"
+++

If you did enjoy some of [my blog posts](@/blog/_index.md), you might want to check out the blogs below too!

- [**while-true-do.io**](https://blog.while-true-do.io): Blog about Linux, FOSS, Self-Hosting and more! {{ feed(link="https://blog.while-true-do.io/rss") }}
- [**SpaceDimp**](https://spacedimp.com/blog): Rust and Linux blog {{ feed(link="https://spacedimp.com/atom.xml") }}
- [**ugvx.dev**](https://ugvx.dev/blog): Rust blog
