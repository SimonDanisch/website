+++
title = "Home | mo8it.com"
+++

# Hello, World!

## Blog

You might find something interesting in my technical [**blog**](@/blog/_index.md) if you are interested in the following topics:

- Rust 🦀
- Linux 🐧
- Free Open Source Software 🔓️
- Self-Hosting 📦

## About me

I am Mo (aka mo8it), a passionate computer nerd. During my bachelor's degree in physics, I realized that I burn for computers. Therefore, I started studying Computational Sciences in my master's degree which is a mix of numerical mathematics, computer science and theoretical physics.

I love to pass my passion about computer topics and hope that you find some of my blog posts and projects helpful 😃

You can follow me on the following accounts:

- 🐘 Mastodon: <a rel="me" href="https://fosstodon.org/@mo8it">@mo8it@fosstodon.org</a>
- 🏔️ Codeberg: [codeberg.org/mo8it](https://codeberg.org/mo8it)

I only use [my Github account](https://github.com/mo8it) to contribute to repositories that aren't mine. My own repositories are on Codeberg 🏔️

## Personal projects

- [Oxiform](https://codeberg.org/mo8it/oxiform): Self-hosted forms for your website written in Rust 🦀 (coming soon)
- [How2Julia](https://codeberg.org/mo8it/How_To_Julia): Notebooks with tasks for a Julia course 🎈
- [How2Linux](https://codeberg.org/mo8it/How_To_Linux): A Linux course 🐧 (not complete yet)
- [Ising GUI](https://codeberg.org/mo8it/ising_gui): Live simulation of Ising algorithms written in Rust 🦀
  - [Ising](https://crates.io/crates/ising): Related library

## Workspace

[Helix](https://helix-editor.com/) 🧬, [GitUI](https://github.com/extrawurst/gitui) and [Fish](https://fishshell.com/) 🐟 inside [Zellij](https://zellij.dev/) on [Fedora Silverblue](https://fedoraproject.org/silverblue/) 🐧
