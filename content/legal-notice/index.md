+++
title = "Legal Notice"
+++

## Hosting

Everything under the domain `mo8it.com` is self-hosted in Germany.

## Address

Mohamad Bitar (Mo)

Jakob-Welder-Weg 30

55128 Mainz

Germany

## E-Mail

mo8it@proton.me
